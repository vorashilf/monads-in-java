package com.company.service;

import com.company.util.Pair;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static java.lang.Double.parseDouble;

public class NormalCalculationService {

    public Pair<String, String> splitInput(String s) {
        List<String> listOfStrings = Arrays.asList(s.split(","));
        return new Pair<>(listOfStrings.get(0), listOfStrings.get(1));

    }

    public Pair<Double, Double> parsePair(Pair<String, String> p) {
        return new Pair<>(parseDouble(p.getLeft()), parseDouble(p.getRight()));
    }

    public Double divideDoubles(Pair<Double, Double> d) {
        return d.getLeft() / d.getRight();
    }
}
