package com.company.service;

import java.util.Optional;

public class EmbellishedDivisionService {

    public Optional<Integer> divide(Integer a, Integer b) {
        return  (b == 0) ? Optional.empty() : Optional.of(a/b);
    }

    public Optional<Integer> addTen(Integer a) {
        return Optional.of(a + 10);
    }
}
