package com.company.service;

import com.company.util.Pair;
import sun.invoke.empty.Empty;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static java.lang.Double.parseDouble;

public class EmbellishedCalculationService {
    public Optional<Pair<String, String>> splitInput(String s) {
        try {
            List<String> listOfStrings = Arrays.asList(s.split(","));
            return Optional.of(new Pair<>(listOfStrings.get(0), listOfStrings.get(1)));
        } catch (ArrayIndexOutOfBoundsException e) {
            return Optional.empty();
        }
    }

    public Optional<Pair<Double, Double>> parsePair(Pair<String, String > p) {
        try {
            return Optional.of(new Pair<>(parseDouble(p.getLeft()), parseDouble(p.getRight())));
        } catch (NumberFormatException n) {
            return Optional.empty();
        }
    }

    public Optional<Double> divideDoubles(Pair<Double,Double> d){
        try {
            return Optional.of(d.getLeft() / d.getRight());
        } catch (Exception e) {
            return Optional.empty();
        }
    }

}
