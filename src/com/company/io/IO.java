package com.company.io;

import java.util.function.Consumer;
import java.util.function.Function;

public class IO<A> {
    private final Effect<A> effect;

    public IO(Effect<A> effect) {
        this.effect = effect;
    }

    public A unsafeRun() {
        return effect.run();
    }

    public static <T> IO<T> apply(Effect<T> effect) {
        return new IO<>(effect);
    }

//    public <B> IO<B> flatMap(Function< A, IO<B>> function) {
//        return IO.apply(() -> function.apply(effect.run()).unsafeRun());
//    }

    public <B> IO<B> flatMap(Function<A, IO<B>> function) {
        Effect<B> compositionWithSideEffect = () -> function.apply(this.effect.run()).unsafeRun();
        return new IO<>(compositionWithSideEffect);
    }


    public <B> IO<B> map(Function<A, B> function){
        return this.flatMap(result -> IO.apply(() -> {
               return function.apply(result);
        }));
    }

    public IO<Void> mapToVoid(Consumer<A> function) {
        return this.flatMap(result -> IO.apply(() -> {
            function.accept(result);
            return null;
        }));
    }

}
