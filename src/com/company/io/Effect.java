package com.company.io;

public interface Effect<T> {
    T run();
}
