package com.company.siso;

import java.util.Objects;
import java.util.function.Function;

public class Siso<A> {

    private final A value;

    private Siso(A value) {
        this.value = Objects.requireNonNull(value);
    }


    public <B> Siso<B> flatMap(Function<A, Siso<B>> function) {
        return function.apply(value);
    }
}
