package com.company;

import com.company.io.Effect;
import com.company.io.IO;
import com.company.service.DivisionService;
import com.company.service.EmbellishedCalculationService;
import com.company.service.EmbellishedDivisionService;
import com.company.service.NormalCalculationService;
import com.company.util.Pair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

public class Main {

    public static void main(String[] args) {
//        Optional<Double> result = calculateDivisionWithEmbellishedTypes("126,0");
//        Optional<Double> result = calculateDivisionMonadically("420,4");
//        Double result = calculateDivision("126,3");

//        Integer result = divideAndAddTen(8,0);
        Optional<Integer> result = divideAndAddTenWithOptionals(8, 0);

        System.out.println(result);
        System.out.println(Optional.of(Optional.of(5)).flatMap(number -> number));

        IO.apply(() -> "What is your name?")
                .mapToVoid(System.out::println)
                .map(ignored -> readFromConsole())
                .map(name -> String.format("Hello, %s", name))
                .mapToVoid(System.out::println)
                .unsafeRun();

        IO<String> askName = new IO<>(() -> "What is your name?");
        Consumer<Void> printToConsole = System.out::println;
        Function<String, Void>


        IO<String> myName = IO.apply(() -> "Vorashil");
        System.out.println(askName.unsafeRun());

    }


    private static String readFromConsole() {
        try {
            BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
            return bufferRead.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static Integer divideAndAddTen(Integer a, Integer b) {
        DivisionService divisionService = new DivisionService();
//        Optional<Integer> sa = map(divisionService.divide, Optional.of(4))
        return divisionService.addTen(divisionService.divide(a, b));
    }

    private static Optional<Integer> divideAndAddTenWithOptionals(Integer a, Integer b) {
        EmbellishedDivisionService embellishedDivisionService = new EmbellishedDivisionService();
        final Optional<Integer> divisionResult = embellishedDivisionService.divide(a, b);
        if (divisionResult.isPresent()) {
            return embellishedDivisionService.addTen(divisionResult.get());
        }
        return Optional.empty();
    }

    private static Optional<Integer> divideAndAddTenWithMonad(Integer a, Integer b) {
        EmbellishedDivisionService embellishedDivisionService = new EmbellishedDivisionService();
        return embellishedDivisionService.divide(a, b)
                .flatMap(divisionResult -> embellishedDivisionService.addTen(divisionResult));
    }


    private static Double calculateDivision(String s) {
        NormalCalculationService calculationService = new NormalCalculationService();
        return calculationService.divideDoubles(calculationService.parsePair(calculationService.splitInput(s)));
    }

    private static Optional<Double> calculateDivisionWithEmbellishedTypes(String s) {
        EmbellishedCalculationService calculationService = new EmbellishedCalculationService();
        final Optional<Pair<String, String>> splittedString = calculationService.splitInput(s);
        if (splittedString.isPresent()) {
            final Optional<Pair<Double, Double>> parsedDoubles = calculationService.parsePair(splittedString.get());
            if (parsedDoubles.isPresent()) {
                return calculationService.divideDoubles(parsedDoubles.get());
            }
        }
        return Optional.empty();
    }

    private static Optional<Double> calculateDivisionMonadically(String s) {
        EmbellishedCalculationService calculationService = new EmbellishedCalculationService();
        return calculationService.splitInput(s)
                .flatMap(split -> calculationService.parsePair(split))
                .flatMap(parse -> calculationService.divideDoubles(parse));
    }
}
